#include <ESP8266WiFi.h>
// LED ON/OFF
// http://<your_ip>/on   <- for LED ON
// http://<your_ip>/off   <- for LED OFF

const char* ssid = "CPE";  
const char* password = "ennpu4.0";  
unsigned char status_led=0; 
WiFiServer server(80);     
 
void setup() {
  Serial.begin(115200); 
  pinMode(LED_BUILTIN, OUTPUT);   
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);  
  while (WiFi.status() != WL_CONNECTED)   
  {
        delay(500);
        Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  server.begin();              
  Serial.println("Server started");
  Serial.println(WiFi.localIP());    
}
 
void loop() {
  WiFiClient client = server.available();  
  if (!client) {   
    return;
  }
   
  Serial.println("new client");
  while(!client.available())
  {
    delay(1);
  }
  String req = client.readStringUntil('\r'); 
  Serial.println(req);      
  client.flush();
  if (req.indexOf("/on") != -1) 
  {
    status_led=1;                  
    digitalWrite(LED_BUILTIN,LOW);      
    Serial.println("LED OFF");
  }
  else if(req.indexOf("/off") != -1)  
  {
    status_led=0;                
    digitalWrite(LED_BUILTIN,HIGH);     
    Serial.println("LED ON");
  }

  String web = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n";
  web += "<html>\r\n";
  web += "<body>\r\n";
  web += "<h1>LED Status</h1>\r\n";
  web += "<p>\r\n";
  if(status_led==1) 
      web += "LED On\r\n";
  else
      web += "LED Off\r\n";
  web += "</p>\r\n";
  web += "</body>\r\n";
  web += "</html>\r\n";
  client.print(web); 
}